/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	InnerBlocks,
	ColorPalette,
	RichText,
} = wp.editor; // Import Rich text editor

const icon = () => {
	return (
		<svg id="Group_262" data-name="Group 262" xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512 512">
			<path id="Path_180" data-name="Path 180" d="M4918.2,3159.68h18.035l11.1,11.539v92.559H4689.045l-47.311-104.1Z" transform="translate(-4444 -3151)" fill="#c8d186" />
			<g id="Rectangle_57" data-name="Rectangle 57" fill="none" stroke="#a1b131" strokeWidth="16">
				<rect width="512" height="512" rx="25" stroke="none" />
				<rect x="8" y="8" width="496" height="496" rx="17" fill="none" />
			</g>
			<line id="Line_16" data-name="Line 16" y2="104" transform="translate(364.5 6.5)" fill="none" stroke="#a1b131" strokeWidth="16" />
			<path id="Path_179" data-name="Path 179" d="M4607.939,3355.316l46.133,103.373H4912.73" transform="translate(-4410 -3344)" fill="none" stroke="#a1b131" strokeWidth="16" />
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/tabbed-content', {
	title: __( 'Tabbed Content' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'Tabbed Content' ),
		__( 'Meta13 meta13' ),
		__( 'tab' ),
	],
	attributes: {
		fontColor: {
			type: 'string',
			default: 'black',
		},
		headerContent: {
			source: 'html',
			selector: 'h1',
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent },
			setAttributes,
		} = props;
		const {
			fontColor,
		} = props.attributes;
		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<RichText
					tagName="h3"
					placeholder={ 'Heading here...' }
					value={ headerContent }
					onChange={ onChangeHeaderContent }
					style={ {
						color: `${ fontColor }`,
					} }
				/>
				<div>
					<h3>Select a font color:</h3>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
			>
				<div className="content">
					<div className="divider">
						<span className="hexagon"></span>
						<span className="hexagon"></span>
						<span className="hexagon"></span>
					</div>
					<RichText
						tagName="h1"
						placeholder={ 'Heading here...' }
						value={ headerContent }
						onChange={ onChangeHeaderContent }
						style={ {
							color: `${ fontColor }`,
						} }
					/>
					<div className="tabbed">
						<InnerBlocks
							allowedBlocks={ [ 'cgb/tabbed-content-item' ] }
						/>
					</div>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			fontColor,
			headerContent,
		} = props.attributes;
		return (
			<div>
				<div className="content">
					<div className="divider">
						<span className="hexagon"></span>
						<span className="hexagon"></span>
						<span className="hexagon"></span>
					</div>
					<h1
						style={ {
							color: `${ fontColor }`,
						} }
					>{ headerContent }</h1>
					<div className="tabbed">
						<InnerBlocks.Content />
					</div>
				</div>
			</div>
		);
	},
} );
