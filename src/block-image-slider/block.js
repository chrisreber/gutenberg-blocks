/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	ColorPalette,
	MediaUpload,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const { RangeControl } = wp.components;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-blocks-image-slider', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Hero Image Block Controls' ), // Block title.
	icon: 'format-image', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		fontColor: {
			type: 'string',
			default: 'black',
		},
		overlayColor: {
			type: 'string',
			default: 'orange',
		},
		backgroundImage: {
			type: 'string',
			default: null,
		},
		overlayOpacity: {
			type: 'number',
			default: '0.3',
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			className,
		} = props;
		const { fontColor, overlayColor, backgroundImage, overlayOpacity } = props.attributes;
		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onImageSelect( imageObject ) {
			setAttributes( {
				backgroundImage: imageObject.sizes.large.url,
			} );
		}
		function onOverlayOpacity( changes ) {
			setAttributes( {
				overlayOpacity: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<RangeControl
						label="Overlay Opacity"
						value={ overlayOpacity }
						onChange={ onOverlayOpacity }
						min={ 0 }
						max={ 1 }
						step={ 0.05 }
					/>
				</div>
				<div>
					<strong>Select a font color:</strong>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
				</div>
				<div>
					<strong>Select an overlay color:</strong>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
				</div>
				<div>
					<strong>Select a background image</strong>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Upload Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
					color: `${ fontColor }`,
				} }>
				<div
					className="overlay"
					style={ {
						backgroundColor: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
				<div className="content">
					<InnerBlocks
						templateLock={ 'all' }
						template={ [
							[ 'core/heading', { placeholder: 'Header' } ],
							[ 'core/paragraph', { placehoder: 'content' } ],
							[ 'core/button', { placeholder: 'learn now' } ],
						] } />
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { overlayColor, fontColor, overlayOpacity, backgroundImage } = props.attributes;
		return (
			<div
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
					color: `${ fontColor }`,
				} }>
				<div
					className="overlay"
					style={ {
						background: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
				<div className="content">
					<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );
