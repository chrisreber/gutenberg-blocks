/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const icon = () => {
	return (
		<svg aria-hidden="true" data-prefix="fas" data-icon="images" role="img" xmlns="http://www.w3.org/2000/svg" viewBox="0 0 576 512" className="svg-inline--fa fa-images fa-w-18 fa-3x">
			<path fill="#f67711" d="M480 416v16c0 26.51-21.49 48-48 48H48c-26.51 0-48-21.49-48-48V176c0-26.51 21.49-48 48-48h16v208c0 44.112 35.888 80 80 80h336zm96-80V80c0-26.51-21.49-48-48-48H144c-26.51 0-48 21.49-48 48v256c0 26.51 21.49 48 48 48h384c26.51 0 48-21.49 48-48zM256 128c0 26.51-21.49 48-48 48s-48-21.49-48-48 21.49-48 48-48 48 21.49 48 48zm-96 144l55.515-55.515c4.686-4.686 12.284-4.686 16.971 0L272 256l135.515-135.515c4.686-4.686 12.284-4.686 16.971 0L512 208v112H160v-48z" className=""></path>
		</svg>
	);
};

export default icon;
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-sponsors', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Sponsors' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'content' ),
		__( 'side image' ),
	],
	edit: function( props ) {
		const {
			className,
		} = props;

		return ( [
			<InspectorControls key="first">

			</InspectorControls>,
			<div
				key="second"
				className={ className }
			>
				<div className="content">
					<InnerBlocks
						templateLock={ 'all' }
						template={ [
							[ 'core/gallery', { placeholder: 'learn now' } ],
						] } />
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { } = props.attributes;
		return (
			<div
				className={ className }
			>
				<div className="content">
					<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );
