/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	RichText,
	InspectorControls,
	ColorPalette,
	MediaUpload,
} = wp.editor; // Import Rich text editor

const { RangeControl } = wp.components;

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-buttons', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'cgb Button' ), // Block title.
	icon: 'minus', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — button' ),
		__( 'button' ),
		__( 'foundation' ),
	],
	attributes: {
		url: {
			type: 'string',
			source: 'attribute',
			selector: 'a',
			attribute: 'href',
		},
		textString: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		fontColor: {
			type: 'string',
			default: 'black',
		},
		overlayColor: {
			type: 'string',
			default: 'orange',
		},
		overlayOpacity: {
			type: 'number',
			default: '0.3',
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			attributes,
			className,
			focus,
		} = props;
		const {
			fontColor,
			overlayColor,
			overlayOpacity,
			url,
		} = props.attributes;
		function onUrlChange( changes ) {
			setAttributes( {
				url: changes,
			} );
		}
		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onTextChange( changes ) {
			setAttributes( {
				textString: changes,
			} );
		}
		function onOverlayOpacity( changes ) {
			setAttributes( {
				overlayOpacity: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<RangeControl
						label="Overlay Opacity"
						value={ overlayOpacity }
						onChange={ onOverlayOpacity }
						min={ 0 }
						max={ 1 }
						step={ 0.05 }
					/>
				</div>
				<hr />
				<div>
					<p><strong>Select a font color:</strong></p>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
				</div>
				<hr />
				<div>
					<p><strong>Select an overlay color:</strong></p>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ className }>
				<div
					className="overlay"
					style={ {
						backgroundColor: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
				<RichText
					tagName="h2"
					className="content"
					value={ attributes.textString }
					onChange={ onTextChange }
					placeholder="Enter header text here!"
					style={ { color: fontColor } }
				/>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			attributes,
			className,
		} = props;
		const { fontColor, overlayColor, overlayOpacity } = props.attributes;
		return (
			<div
				className={ className }>
				<div
					className="overlay"
					style={ {
						background: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
				<h2 className="content" style={ { color: fontColor } }>{ attributes.textString }</h2>
			</div>
		);
	},
} );
