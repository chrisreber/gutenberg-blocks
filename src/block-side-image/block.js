/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	ColorPalette,
	MediaUpload,
	RichText,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const {
	FormToggle,
	CheckboxControl,
} = wp.components;

const icon = () => {
	return (
		<svg width="147px" height="54px" viewBox="0 0 147 54">
			<title>Group</title>
			<desc>Created with Sketch.</desc>
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group" fillRule="nonzero">
					<rect id="Rectangle" stroke="#f67711" strokeWidth="3" x="2" y="1.5" width="51" height="51"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="6" width="74" height="8"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="22" width="50" height="5"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="32" width="50" height="5"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="42" width="50" height="5"></rect>
					<path d="M3.5,2.5 L51.5,51.5" id="Line" stroke="#f67711" strokeWidth="3" strokeLinecap="square"></path>
					<path d="M3.5,3.5 L51.5,50.5" id="Line" stroke="#f67711" strokeWidth="3" strokeLinecap="square" transform="translate(27.500000, 27.000000) scale(1, -1) translate(-27.500000, -27.000000) "></path>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-side-image', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Content with side image' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'content' ),
		__( 'side image meta13' ),
	],
	attributes: {
		fontColor: {
			type: 'string',
			default: 'white',
		},
		overlayColor: {
			type: 'string',
			default: 'black',
		},
		backgroundImage: {
			type: 'string',
			default: null,
		},
		backgroundImageAlt: {
			attribute: 'alt',
			selector: 'img',
			source: 'attribute',
			type: 'string',
			default: '',
		},
		isSlider: {
			type: 'boolean',
			default: false,
		},
		headerContent: {
			source: 'html',
			selector: 'h2',
		},
		paragraphContent: {
			source: 'html',
			selector: 'p',
		},
		wideMode: {
			type: 'boolean',
			default: false,
		},
		fontLarge: {
			type: 'boolean',
			default: false,
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent, paragraphContent },
			setAttributes,
			className,
		} = props;
		const {
			fontColor,
			overlayColor,
			backgroundImage,
			backgroundImageAlt,
			isSlider,
			wideMode,
			fontLarge,
		} = props.attributes;

		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onImageSelect( imageObject ) {
			setAttributes( {
				backgroundImage: imageObject.sizes.large.url,
				backgroundImageAlt: imageObject.alt,
			} );
		}
		function onFormChange() {
			setAttributes( {
				isSlider: ! isSlider,
			} );
		}
		function onWideChange() {
			setAttributes( {
				wideMode: ! wideMode,
			} );
		}
		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		function onChangeParagraphContent( changes ) {
			setAttributes( {
				paragraphContent: changes,
			} );
		}
		function onFontLargeChange() {
			setAttributes( {
				fontLarge: ! fontLarge,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<hr />
				<div>
					<h3>Select an background color:</h3>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
				</div>
				<hr />
				<div>
					<h3>Select a font color:</h3>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
					<CheckboxControl
						heading="Font Large Size:"
						label="Large"
						help="Change the font size to large?"
						checked={ fontLarge }
						onChange={ onFontLargeChange }
					/>
				</div>
				<hr />
				<div>
					<h3>Select a side image:</h3>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Select Image!
							</button>
						) }
					/>
				</div>
				<hr />
				<div>
					<h3>Flip content direction:</h3>
					<FormToggle
						label="Toggle"
						checked={ isSlider }
						onChange={ onFormChange }
					/>
				</div>
				<hr />
				<div>
					<h3>Wide Mode:</h3>
					<FormToggle
						label="Toggle"
						checked={ wideMode }
						onChange={ onWideChange }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className } ${ isSlider === true ? 'image-right' : 'image-left' } ` }
				style={ {
					color: `${ fontColor }`,
					backgroundColor: overlayColor,
				} }>
				<div className={ `grid-column ${ wideMode === true ? 'grid-column__wide' : 'grid-column__narrow' } ` } >
					<div className="content">
						<RichText
							tagName="h2"
							placeholder={ 'Heading here...' }
							value={ headerContent }
							onChange={ onChangeHeaderContent }
							style={ {
								color: `${ fontColor }`,
							} }
						/>
						<RichText
							tagName="p"
							className={ fontLarge === true ? 'font-size__large' : 'font-size__small' }
							placeholder={ 'Content here...' }
							value={ paragraphContent }
							onChange={ onChangeParagraphContent }
						/>
						<InnerBlocks
							allowedBlocks={ [
								'core/button',
								'core/paragraph',
								'core/list',
							] }
							templateLock="insert"
						>
						</InnerBlocks>
					</div>
					<div className="image">
						<img
							src={ backgroundImage }
							alt={ backgroundImageAlt }
						/>
					</div>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { overlayColor, fontColor, backgroundImage, isSlider, headerContent, paragraphContent, wideMode, fontLarge, backgroundImageAlt } = props.attributes;
		return (
			<div
				className={ `${ className } ${ isSlider === true ? 'image-right' : 'image-left' } ${ wideMode === true ? 'content-wide' : 'content-narrow' } ` }
				style={ {
					backgroundColor: overlayColor,
					color: `${ fontColor }`,
				} }>
				<div className={ ` grid-column ${ wideMode === true ? 'grid-column__wide' : 'grid-column__narrow' } ` }>
					<div className="content">
						<h2
							style={ {
								color: `${ fontColor }`,
							} }
						>{ headerContent }</h2>
						<p
							className={ fontLarge === true ? 'font-size__large' : 'font-size__small' }
						>{ paragraphContent }</p>

						<InnerBlocks.Content />
					</div>
					<div className="image">
						<img
							src={ backgroundImage }
							alt={ backgroundImageAlt }
						/>
					</div>
				</div>
			</div>
		);
	},
	deprecated: [
		{
			save( props ) {
				const {
					className,
				} = props;
				const { overlayColor, fontColor, backgroundImage, isSlider, headerContent, paragraphContent, wideMode, fontLarge, backgroundImageAlt } = props.attributes;
				return (
					<div
						className={ `${ className } ${ isSlider === true ? 'image-right' : 'image-left' } ${ wideMode === true ? 'content-wide' : 'content-narrow' } ` }
						style={ {
							backgroundColor: overlayColor,
							color: `${ fontColor }`,
						} }>
						<div className={ ` grid-column ${ wideMode === true ? 'grid-column__wide' : 'grid-column__narrow' } ` }>
							<div className="content">
								<h2
									style={ {
										color: `${ fontColor }`,
									} }
								>{ headerContent }</h2>
								<p
									className={ fontLarge === true ? 'font-size__large' : 'font-size__small' }
								>{ paragraphContent }</p>
								<InnerBlocks.Content />
							</div>
							<div className="image">
								<img
									src={ backgroundImage }
									alt={ backgroundImageAlt }
								/>
							</div>
						</div>
					</div>
				);
			},
		},
	],
} );
