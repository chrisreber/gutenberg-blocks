/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	ColorPalette,
	MediaUpload,
	RichText,
} = wp.editor; // Import Rich text editor

const { RangeControl } = wp.components;

const icon = () => {
	return (
		<svg width="113px" height="94px" viewBox="0 0 113 94">
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group-2" transform="translate(8.000000, 8.000000)" fillRule="nonzero">
					<rect id="Rectangle" stroke="#F67711" strokeWidth="8" x="-4" y="-4" width="105" height="86" rx="1"></rect>
					<path d="M23.5,17.5 L75.3613281,17.5" id="Line-2" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round"></path>
					<path d="M23.5,37.5 L75.3613281,37.5" id="Line-2" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round"></path>
					<circle id="Oval" fill="#F67711" cx="25" cy="59" r="6"></circle>
					<circle id="Oval" fill="#F67711" cx="49" cy="59" r="6"></circle>
					<circle id="Oval" fill="#F67711" cx="73" cy="59" r="6"></circle>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-slide-image', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Slide Image and Text' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Slideshow' ),
		__( 'Meta13 meta13' ),
		__( 'slider slide carousel' ),
	],
	attributes: {
		fontColor: {
			type: 'string',
			default: 'black',
		},
		overlayColor: {
			type: 'string',
			default: 'black',
		},
		backgroundImage: {
			type: 'string',
			default: null,
		},
		backgroundImageAlt: {
			attribute: 'alt',
			selector: 'img',
			source: 'attribute',
			type: 'string',
			default: '',
		},
		headerContent: {
			source: 'html',
			selector: 'h1',
		},
		overlayOpacity: {
			type: 'number',
			default: '0.0',
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent },
			setAttributes,
			className,
		} = props;
		const {
			fontColor,
			overlayColor,
			backgroundImage,
			backgroundImageAlt,
			overlayOpacity,
		} = props.attributes;

		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onImageSelect( imageObject ) {
			if ( imageObject.alt ) {
				setAttributes( {
					backgroundImage: imageObject.sizes.large.url,
					backgroundImageAlt: imageObject.alt,
				} );
			} else {
				setAttributes( {
					backgroundImage: imageObject.sizes.large.url,
					backgroundImageAlt: '',
				} );
			}
		}
		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		function onOverlayOpacity( changes ) {
			setAttributes( {
				overlayOpacity: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<RichText
					tagName="h3"
					placeholder={ 'Heading here...' }
					value={ headerContent }
					onChange={ onChangeHeaderContent }
					style={ {
						color: `${ fontColor }`,
					} }
				/>
				<hr />
				<div>
					<h3>Select an overlay Color color (-optional):</h3>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
					<RangeControl
						label="Overlay Opacity"
						value={ overlayOpacity }
						onChange={ onOverlayOpacity }
						min={ 0 }
						max={ 1 }
						step={ 0.05 }
					/>
				</div>
				<hr />
				<div>
					<h3>Select a font color:</h3>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
				</div>
				<hr />
				<div>
					<h3>Select a side image:</h3>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Select Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className }` }
				style={ {
					color: `${ fontColor }`,
				} }>
				<div className="overlay"
					style={ {
						backgroundColor: overlayColor,
						opacity: overlayOpacity,
					} }
				>
				</div>
				<div className="content">
					<RichText
						tagName="h1"
						placeholder={ 'Heading here...' }
						value={ headerContent }
						onChange={ onChangeHeaderContent }
						style={ {
							color: `${ fontColor }`,
						} }
					/>
				</div>
				<div className="image">
					<img
						src={ backgroundImage }
						alt={ backgroundImageAlt }
					/>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { overlayColor, fontColor, backgroundImage, headerContent, backgroundImageAlt, overlayOpacity } = props.attributes;
		return (
			<div
				className={ `${ className }` }
				style={ {
					color: `${ fontColor }`,
				} }>
				<div className="overlay"
					style={ {
						backgroundColor: overlayColor,
						opacity: overlayOpacity,
					} }
				>
				</div>
				<div className="content">
					<h1
						style={ {
							color: `${ fontColor }`,
						} }
					>{ headerContent }</h1>
				</div>
				<div className="image">
					<img
						src={ backgroundImage }
						alt={ backgroundImageAlt }
					/>
				</div>
			</div>
		);
	},
} );
