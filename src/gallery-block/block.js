/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	RichText,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const { RangeControl } = wp.components;

const icon = () => {
	return (
		<svg width="525px" height="532px" viewBox="0 0 525 532">
			<g id="r" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group" fill="#F67711" fillRule="nonzero">
					<rect id="Rectangle" x="0" y="369" width="159" height="163"></rect>
					<rect id="Rectangle" x="183" y="369" width="159" height="163"></rect>
					<rect id="Rectangle" x="366" y="369" width="159" height="163"></rect>
					<rect id="Rectangle" x="0" y="186" width="159" height="163"></rect>
					<rect id="Rectangle" x="183" y="186" width="159" height="163"></rect>
					<rect id="Rectangle" x="366" y="186" width="159" height="163"></rect>
					<rect id="Rectangle" x="136" y="0" width="253" height="58"></rect>
					<rect id="Rectangle" x="0" y="79" width="525" height="31"></rect>
					<rect id="Rectangle" x="0" y="122" width="525" height="31"></rect>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-gallery-block', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Gallery Block' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'gallery block' ),
		__( 'meta13 image' ),
	],
	attributes: {
		backgroundImage: {
			type: 'string',
			default: null,
		},
		backgroundImageAlt: {
			attribute: 'alt',
			selector: 'img',
			source: 'attribute',
			type: 'string',
			default: '',
		},
		headerContent: {
			source: 'html',
			selector: 'h2',
		},
		paragraphContent: {
			source: 'html',
			selector: 'p',
		},
		gridLayout: {
			type: 'number',
			default: '2',
		},
		grid: {
			type: 'string',
			default: 'column__two',
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent, paragraphContent },
			setAttributes,
			className,
		} = props;
		const { gridLayout } = props.attributes;
		let {
			attributes: { grid },
		} = props;

		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		function onChangeParagraphContent( changes ) {
			setAttributes( {
				paragraphContent: changes,
			} );
		}
		function onGridLayout( changes ) {
			let grids;
			if ( changes === 1 ) {
				grids = 'column__one';
			} else if ( changes === 2 ) {
				grids = 'column__two';
			} else if ( changes === 3 ) {
				grids = 'column__three';
			} else {
				grids = 'column__four';
			}
			setAttributes( {
				grid: grids,
				gridLayout: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<RangeControl
						label="Overlay Opacity"
						value={ gridLayout }
						onChange={ onGridLayout }
						min={ 1 }
						max={ 4 }
						step={ 1 }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className } ` } >
				<div className={ 'grid-column' } >
					<div className="content">
						<RichText
							tagName="h2"
							placeholder={ 'Heading here...' }
							value={ headerContent }
							onChange={ onChangeHeaderContent }
						/>
						<RichText
							tagName="p"
							placeholder={ 'Content here...' }
							value={ paragraphContent }
							onChange={ onChangeParagraphContent }
						/>
						<div className={ ` ${ grid } ${ 'content-grid' } ` }>
							<InnerBlocks
								allowedBlocks={ [ 'cgb/block-nettwork-gallery-block-image' ] }
							>
							</InnerBlocks>
						</div>
					</div>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { headerContent, paragraphContent, grid } = props.attributes;
		return (
			<div
				className={ `${ className } ` } >
				<div className={ 'grid-column' }>
					<div className="content">
						<h2>{ headerContent }</h2>
						<p>{ paragraphContent }</p>
						<div className={ ` ${ grid } ${ 'content-grid' } ` }>
							<InnerBlocks.Content />
						</div>
					</div>
				</div>
			</div>
		);
	},
} );
