/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	RichText,
	InspectorControls,
	ColorPalette,
	MediaUpload,
} = wp.editor; // Import Rich text editor

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-blocks-image', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Hero Image Block' ), // Block title.
	icon: 'format-image', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'CGB Example' ),
		__( 'create-guten-block' ),
	],
	attributes: {
		textString: {
			type: 'array',
			source: 'children',
			selector: 'h2',
		},
		fontColor: {
			type: 'string',
			default: 'black',
		},
		overlayColor: {
			type: 'string',
			default: 'orange',
		},
		backgroundImage: {
			type: 'string',
			default: null,
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			attributes,
			className,
			focus,
		} = props;
		const { fontColor, overlayColor, backgroundImage } = props.attributes;
		function onTextColorChange( changes ) {
			setAttributes( {
				fontColor: changes,
			} );
		}
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onTextChange( changes ) {
			setAttributes(
				{
					textString: changes,
				}
			);
		}
		function onImageSelect( imageObject ) {
			setAttributes( {
				backgroundImage: imageObject.sizes.full.url,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<strong>Select a font color:</strong>
					<ColorPalette
						value={ fontColor }
						onChange={ onTextColorChange }
					/>
				</div>
				<div>
					<strong>Select an overlay color:</strong>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
				</div>
				<div>
					<strong>Select a background image</strong>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Upload Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div
					className="overlay"
					style={ { background: overlayColor } }
				></div>
				<RichText
					tagName="h2"
					className="content"
					value={ attributes.textString }
					onChange={ onTextChange }
					placeholder="Enter your text here!"
					style={ { color: fontColor } }
				/>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			attributes,
			className,
		} = props;
		const { fontColor, overlayColor, backgroundImage } = props.attributes;
		return (
			<div
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div
					className="overlay"
					style={ { background: overlayColor } }
				></div>
				<h2 className="content" style={ { color: fontColor } }>{ attributes.textString }</h2>
			</div>
		);
	},
} );
