/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	MediaUpload,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/meaning-purpose-participants', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Participants' ), // Block title.
	icon: 'format-image', // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'meaning and purpose' ),
		__( 'participants' ),
		__( 'meta13 meta 13' ),
	],
	attributes: {
		backgroundImage: {
			type: 'string',
			default: null,
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			className,
		} = props;
		const { backgroundImage } = props.attributes;
		function onImageSelect( imageObject ) {
			setAttributes( {
				backgroundImage: imageObject.sizes.full.url,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<strong>Select a background image</strong>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Upload Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div className="content">
					<h3>Add participants below:</h3>
					<h4>Each participant gets it&apos;s own paragraph.</h4>
					<hr />
					<InnerBlocks
						// templateLock={ 'all' }
						template={ [
							[ 'core/paragraph', { placehoder: 'content' } ],
						] }
						allowedBlocks={ [ 'cgb/block-nettwork-slide-image' ] } />
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { backgroundImage } = props.attributes;
		return (
			<div
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div className="content">
					<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );
