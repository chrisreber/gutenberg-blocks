/**
 * Gutenberg Blocks
 *
 * All blocks related JavaScript files should be imported here.
 * You can create a new block folder in this dir and include code
 * for that block here as well.
 *
 * All blocks should be included here since this is the file that
 * Webpack is compiling as the input file.
 */

// import './block/block.js';
// import './block-image-slider/block.js';
// import './block-button/block.js';
// import './button-test/block.js';
// import './block-side-image/block.js';
// import './block-side-video/block.js';
// import './sponsors/block.js';
// import './block-bg-simple/block.js';
// import './gallery-block/block.js';
// import './gallery-block-image/block.js';
import './content-width/block.js';
import './block-bg/block.js';
import './tabbed-content/block.js';
import './tabbed-content-item/block.js';
