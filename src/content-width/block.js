/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const icon = () => {
	return (
		<svg width="123px" height="94px" viewBox="0 0 123 94" >
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group-3" transform="translate(8.000000, 8.000000)" fillRule="nonzero" stroke="#F67711" strokeWidth="8">
					<rect id="Rectangle" fill="#FFFFFF" x="-4" y="-4" width="115" height="86"></rect>
					<path d="M75.5,12.5 L75.5,65.6337891" id="Line-5" strokeLinecap="square"></path>
					<path d="M34,12.5 L34,65.6337891" id="Line-4" strokeLinecap="square"></path>
				</g>
			</g>
		</svg>
	);
};

export default icon;
const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const {
	SelectControl,
} = wp.components;
/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-content-width', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Content Width' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'meta13 meta 13' ),
		__( 'content' ),
		__( 'column grid width' ),
	],
	attributes: {
		width: {
			type: 'string',
			default: 'grid-width__two-thirds',
		},
		align: {
			type: 'string',
			default: 'grid-width__center',
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			className,
		} = props;
		const {
			width,
			align,
		} = props.attributes;
		function onChangeWidth( changes ) {
			setAttributes( {
				width: changes,
			} );
		}
		function onChangeAlign( changes ) {
			setAttributes( {
				align: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<SelectControl
					label="Size"
					value={ width }
					options={ [
						{ label: 'Two thirds - 2/3', value: 'grid-width__two-thirds' },
						{ label: 'Half', value: 'grid-width__half' },
					] }
					onChange={ onChangeWidth }
				/>
				<hr />
				<SelectControl
					label="Alignment"
					value={ align }
					options={ [
						{ label: 'Left', value: 'grid-width__left' },
						{ label: 'Center', value: 'grid-width__center' },
						{ label: 'Right', value: 'grid-width__right' },
					] }
					onChange={ onChangeAlign }
				/>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className } ${ width } ${ align }` }
			>
				<div className="content">
					<InnerBlocks />
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const {
			width,
			align,
		} = props.attributes;
		return (
			<div
				className={ `${ className } ${ width } ${ align }` }
			>
				<div className="content">
					<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );
