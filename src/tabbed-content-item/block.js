/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	RichText,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const icon = () => {
	return (
		<svg xmlns="http://www.w3.org/2000/svg" width="512" height="512" viewBox="0 0 512 512">
			<g id="Group_263" data-name="Group 263" transform="translate(1)">
				<path id="Path_181" data-name="Path 181" d="M4918.2,3159.68h18.035l11.1,11.539v92.559H4689.045l-47.311-104.1Z" transform="translate(-4445 -3151)" fill="#c8d186" />
				<g id="Rectangle_58" data-name="Rectangle 58" transform="translate(-1)" fill="none" stroke="#a1b131" strokeWidth="16">
					<rect width="512" height="512" rx="25" stroke="none" />
					<rect x="8" y="8" width="496" height="496" rx="17" fill="none" />
				</g>
				<line id="Line_17" data-name="Line 17" y2="104" transform="translate(363.5 6.5)" fill="none" stroke="#a1b131" strokeWidth="16" />
				<path id="Path_182" data-name="Path 182" d="M4607.939,3355.316l46.133,103.373H4912.73" transform="translate(-4411 -3344)" fill="none" stroke="#a1b131" strokeWidth="16" />
				<path id="Path_183" data-name="Path 183" d="M3848.9,2705.073h239.263L4194.586,2811.5l-106.425,106.425H3848.9Z" transform="translate(-3755 -2493.547)" fill="#c8d186" stroke="#a1b131" strokeLinejoin="round" strokeWidth="16" />
				<path id="Union_1" data-name="Union 1" d="M65.5,173V107.5H0v-42H65.5V0h42V65.5H173v42H107.5V173Z" transform="translate(134.5 230)" fill="#fff" />
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/tabbed-content-item', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Tabbed Content Item' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'Tabbed Content Item' ),
		__( 'Meta13 meta13' ),
		__( 'tab' ),
	],
	attributes: {
		headerContent: {
			type: 'string',
			source: 'text',
			selector: 'h2',
		},
		tabTitle: {
			type: 'string',
			source: 'text',
			selector: 'h3',
		},
		tabDate: {
			type: 'string',
			source: 'text',
			selector: 'p',
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent, tabTitle, tabDate },
			setAttributes,
		} = props;
		const {
		} = props.attributes;

		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		function onChangeTabTitle( changes ) {
			setAttributes( {
				tabTitle: changes,
			} );
		}
		function onChangeTabDate( changes ) {
			setAttributes( {
				tabDate: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">

				<h2>Tab:</h2>
				<RichText
					tagName="h3"
					placeholder={ 'Tab Title...' }
					value={ tabTitle }
					onChange={ onChangeTabTitle }
				/>
				<RichText
					tagName="p"
					placeholder={ 'Tab Date...' }
					value={ tabDate }
					onChange={ onChangeTabDate }
				/>
				<hr />
				<h2>Contents:</h2>
				<RichText
					tagName="h2"
					placeholder={ 'Heading here...' }
					value={ headerContent }
					onChange={ onChangeHeaderContent }
				/>
			</InspectorControls>,
			<div
				key="second"
			>
				<div className="tabbed-tab">
					<RichText
						tagName="h3"
						placeholder={ 'Tab Title...' }
						value={ tabTitle }
						onChange={ onChangeTabTitle }
					/>
					<RichText
						tagName="p"
						placeholder={ 'Tab Date...' }
						value={ tabDate }
						onChange={ onChangeTabDate }
					/>
				</div>
				<div className="content">
					<RichText
						tagName="h2"
						placeholder={ 'Heading here...' }
						value={ headerContent }
						onChange={ onChangeHeaderContent }
					/>
					<InnerBlocks />
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const { headerContent, tabTitle, tabDate } = props.attributes;
		return (
			<div>
				<div className="tabbed-tab">
					<h3>{ tabTitle }</h3>
					<p>{ tabDate }</p>
				</div>
				<div className="content">
					<h2>{ headerContent }</h2>
					<InnerBlocks.Content />
				</div>
			</div>
		);
	},
} );
