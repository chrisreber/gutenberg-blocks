/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	RichText,
	MediaUpload,
	URLInputButton,
} = wp.editor; // Import Rich text editor

const icon = () => {
	return (
		<svg width="147px" height="54px" viewBox="0 0 147 54">
			<title>Group</title>
			<desc>Created with Sketch.</desc>
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group" fillRule="nonzero">
					<rect id="Rectangle" stroke="#f67711" strokeWidth="3" x="2" y="1.5" width="51" height="51"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="6" width="74" height="8"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="22" width="50" height="5"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="32" width="50" height="5"></rect>
					<rect id="Rectangle" fill="#f67711" x="73" y="42" width="50" height="5"></rect>
					<path d="M3.5,2.5 L51.5,51.5" id="Line" stroke="#f67711" strokeWidth="3" strokeLinecap="square"></path>
					<path d="M3.5,3.5 L51.5,50.5" id="Line" stroke="#f67711" strokeWidth="3" strokeLinecap="square" transform="translate(27.500000, 27.000000) scale(1, -1) translate(-27.500000, -27.000000) "></path>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-gallery-block-image', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Gallery block image' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'nettwork-blocks — CGB Block' ),
		__( 'gallery block' ),
		__( 'Meta13 image' ),
	],
	attributes: {
		backgroundImage: {
			type: 'string',
			default: null,
		},
		backgroundImageAlt: {
			attribute: 'alt',
			selector: 'img',
			source: 'attribute',
			type: 'string',
			default: '',
		},
		headerContent: {
			source: 'html',
			selector: 'h3',
		},
		paragraphContent: {
			source: 'html',
			selector: 'p',
			default: '',
		},
		url: {
			type: 'string',
			default: '#',
		},
	},
	edit: function( props ) {
		const {
			attributes: { headerContent, paragraphContent },
			setAttributes,
			className,
		} = props;
		const {
			url,
			backgroundImage,
			backgroundImageAlt,
		} = props.attributes;

		function onChangeHeaderContent( changes ) {
			setAttributes( {
				headerContent: changes,
			} );
		}
		function onChangeParagraphContent( changes ) {
			setAttributes( {
				paragraphContent: changes,
			} );
		}
		function onImageSelect( imageObject ) {
			if ( imageObject.alt ) {
				setAttributes( {
					backgroundImage: imageObject.sizes.large.url,
					backgroundImageAlt: imageObject.alt,
				} );
			} else {
				setAttributes( {
					backgroundImage: imageObject.sizes.large.url,
					backgroundImageAlt: '',
				} );
			}
		}
		function onChangeUrlText( changes ) {
			setAttributes( {
				url: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div className="editor__overflow-hidden">
					<URLInputButton
						url={ url }
						value={ url }
						onChange={ onChangeUrlText }
					/>
					<RichText
						tagName="a"
						href={ url }
						title={ 'Link' }
						value={ url }
						placeholder={ 'Link here' }
					/>
				</div>
				<hr />
				<RichText
					tagName="h3"
					placeholder={ 'Image Caption here...' }
					value={ headerContent }
					onChange={ onChangeHeaderContent }
				/>
				<hr />
				<RichText
					tagName="p"
					placeholder={ 'Hover content here...' }
					value={ paragraphContent }
					onChange={ onChangeParagraphContent }
				/>
				<hr />
				<div>
					<h3>Select a side image:</h3>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Select Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className } ` } >
				<div className="content">
					<div className="image">
						<img
							src={ backgroundImage }
							alt={ backgroundImageAlt }
						/>
					</div>
					<URLInputButton
						url={ url }
						value={ url }
						onChange={ onChangeUrlText }
					/>
					<RichText
						tagName="h3"
						placeholder={ 'Image Caption here...' }
						value={ headerContent }
						onChange={ onChangeHeaderContent }
					/>
					<RichText
						tagName="p"
						placeholder={ 'Hover content here...' }
						value={ paragraphContent }
						onChange={ onChangeParagraphContent }
					/>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { headerContent, paragraphContent, backgroundImage, backgroundImageAlt, url } = props.attributes;
		function urlornot() {
			if ( url === '#' ) {
				return (
					<div className={ `content ${ paragraphContent.trim() === '' ? '' : 'content-hover' } ` }>
						<div className="image">
							<img
								src={ backgroundImage }
								alt={ backgroundImageAlt }
							/>
						</div>
						<h3>{ headerContent }</h3>
						<div className="content__hover">
							<p>{ paragraphContent }</p>
						</div>
					</div>
				);
			}
			return (
				<a href={ url } className={ `content ${ paragraphContent.trim() === '' ? '' : 'content-hover' } ` }>
					<div className="image">
						<img
							src={ backgroundImage }
							alt={ backgroundImageAlt }
						/>
					</div>
					<h3>{ headerContent }</h3>
					<div className="content__hover">
						<p>{ paragraphContent }</p>
					</div>
				</a>
			);
		}
		return (
			<div
				className={ `${ className } ` } >
				{ urlornot() }
			</div>
		);
	},
} );
