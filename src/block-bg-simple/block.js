/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */

//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	ColorPalette,
	MediaUpload,
} = wp.editor; // Import Rich text editor

const { RangeControl } = wp.components;

const icon = () => {
	return (
		<svg width="123px" height="94px" viewBox="0 0 123 94">
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group-2" transform="translate(8.000000, 8.000000)" fillRule="nonzero">
					<rect id="Rectangle" stroke="#F67711" strokeWidth="8" fill="#FFFFFF" x="-4" y="-4" width="115" height="86"></rect>
					<polygon id="Path" fill="#F67711" points="7 68.9258398 37.0187012 36.379248 47.7622363 47.7547559 67.3533887 19 100.26966 68.9258398"></polygon>
					<circle id="Oval" fill="#F67711" cx="19" cy="18" r="12"></circle>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-blocks-bg-simple', {
	// Block name. Block names must be string that contains a namespace prefix. Example: my-plugin/my-custom-block.
	title: __( 'Simple Background' ), // Block title.
	icon: icon, // Block icon from Dashicons → https://developer.wordpress.org/resource/dashicons/.
	category: 'common', // Block category — Group blocks together based on common traits E.g. common, formatting, layout widgets, embed.
	keywords: [
		__( 'hero' ),
		__( 'background image' ),
		__( 'meta13' ),
	],
	attributes: {
		overlayColor: {
			type: 'string',
			default: 'orange',
		},
		backgroundImage: {
			type: 'string',
			default: null,
		},
		overlayOpacity: {
			type: 'number',
			default: '0.0',
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			className,
		} = props;
		const { overlayColor, backgroundImage, overlayOpacity } = props.attributes;
		function onOverlayColorChange( changes ) {
			setAttributes( {
				overlayColor: changes,
			} );
		}
		function onImageSelect( imageObject ) {
			setAttributes( {
				backgroundImage: imageObject.sizes.full.url,
			} );
		}
		function onOverlayOpacity( changes ) {
			setAttributes( {
				overlayOpacity: changes,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<RangeControl
						label="Overlay Opacity"
						value={ overlayOpacity }
						onChange={ onOverlayOpacity }
						min={ 0 }
						max={ 1 }
						step={ 0.05 }
					/>
				</div>
				<div>
					<strong>Select an overlay color:</strong>
					<ColorPalette
						value={ overlayColor }
						onChange={ onOverlayColorChange }
					/>
				</div>
				<div>
					<strong>Select a background image</strong>
					<MediaUpload
						onSelect={ onImageSelect }
						type="image"
						value={ backgroundImage }
						render={ ( { open } ) => (
							<button onClick={ open }>
								Upload Image!
							</button>
						) }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div
					className="overlay"
					style={ {
						backgroundColor: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const { overlayColor, overlayOpacity, backgroundImage } = props.attributes;
		return (
			<div
				className={ className }
				style={ {
					backgroundImage: `url(${ backgroundImage })`,
					backgroundSize: 'cover',
					backgroundPosition: 'center',
				} }>
				<div
					className="overlay"
					style={ {
						background: overlayColor,
						opacity: overlayOpacity,
					} }
				></div>
			</div>
		);
	},
} );
