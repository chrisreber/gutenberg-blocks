/**
 * BLOCK: nettwork-blocks-image
 *
 * Registering a basic block with Gutenberg.
 * Simple block, renders and saves the same content without any interactivity.
 */
//  Import CSS.
import './style.scss';
import './editor.scss';

const { __ } = wp.i18n; // Import __() from wp.i18n
const { registerBlockType } = wp.blocks; // Import registerBlockType() from wp.blocks
const {
	InspectorControls,
	InnerBlocks,
} = wp.editor; // Import Rich text editor

const {
	CheckboxControl,
} = wp.components;

const icon = () => {
	return (
		<svg width="180px" height="94px" viewBox="0 0 180 94">
			<g id="icons" stroke="none" strokeWidth="1" fill="none" fillRule="evenodd">
				<g id="Group-2" transform="translate(5.000000, 8.000000)" fillRule="nonzero">
					<rect id="Rectangle" stroke="#F67711" strokeWidth="8" x="33" y="-4" width="105" height="86" rx="1"></rect>
					<path d="M60.5,17.5 L112.361328,17.5" id="Line-2" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round"></path>
					<path d="M60.5,37.5 L112.361328,37.5" id="Line-2" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round"></path>
					<circle id="Oval" fill="#F67711" cx="62" cy="59" r="6"></circle>
					<circle id="Oval" fill="#F67711" cx="86" cy="59" r="6"></circle>
					<circle id="Oval" fill="#F67711" cx="110" cy="59" r="6"></circle>
					<path d="M14,29 L0,43 L14,29 Z M14.0849609,57.1259766 L0.0849609375,43.1259766 L14.0849609,57.1259766 Z" id="Combined-Shape" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round"></path>
					<path d="M170,29 L156,43 L170,29 Z M170.084961,57.1259766 L156.084961,43.1259766 L170.084961,57.1259766 Z" id="Combined-Shape" stroke="#F67711" strokeWidth="8" strokeLinecap="round" strokeLinejoin="round" transform="translate(163.042480, 43.062988) scale(-1, 1) translate(-163.042480, -43.062988) "></path>
				</g>
			</g>
		</svg>
	);
};

/**
 * Register: aa Gutenberg Block.
 *
 * Registers a new block provided a unique name and an object defining its
 * behavior. Once registered, the block is made editor as an option to any
 * editor interface where blocks are implemented.
 *
 * @link https://wordpress.org/gutenberg/handbook/block-api/
 * @param  {string}   name     Block name.
 * @param  {Object}   settings Block settings.
 * @return {?WPBlock}          The block, if it has been successfully
 *                             registered; otherwise `undefined`.
 */
registerBlockType( 'cgb/block-nettwork-slideshow', {
	title: __( 'Slideshow' ),
	icon: icon,
	category: 'common',
	keywords: [
		__( 'Slideshow' ),
		__( 'Meta13 meta13' ),
		__( 'slider slide' ),
	],
	attributes: {
		heroSmall: {
			type: 'boolean',
			default: false,
		},
	},
	edit: function( props ) {
		const {
			setAttributes,
			className,
		} = props;
		const {
			heroSmall,
		} = props.attributes;
		function onheroSmallChange() {
			setAttributes( {
				heroSmall: ! heroSmall,
			} );
		}
		return ( [
			<InspectorControls key="first">
				<div>
					<CheckboxControl
						heading="Small hero size:"
						label="small"
						help="Change hero size to small?"
						checked={ heroSmall }
						onChange={ onheroSmallChange }
					/>
				</div>
			</InspectorControls>,
			<div
				key="second"
				className={ `${ className } ${ heroSmall === true ? 'hero-small' : 'hero-large' } ` }
			>
				<div className="content">
					<h4>Add Slides below: <small> - (this line is only visible in the editor)</small></h4>
					<div className="slideshow">
						<InnerBlocks
							allowedBlocks={ [ 'cgb/block-nettwork-slide-image' ] }
						/>
					</div>
				</div>
			</div>,
		] );
	},
	save: function( props ) {
		const {
			className,
		} = props;
		const {
			heroSmall,
		} = props.attributes;
		return (
			<div
				className={ `${ className } ${ heroSmall === true ? 'hero-small' : 'hero-large' } ` }
			>
				<div className="content">
					<div className="slideshow">
						<InnerBlocks.Content />
					</div>
				</div>
			</div>
		);
	},
} );
