<?php
/**
 * Plugin Name: Meta13 Blocks
 * Plugin URI: https://meta13.com
 * Description: Meta13 blocks — is a Gutenberg plugin created via create-guten-block.
 * Author: Meta13 Chris Reber
 * Author URI: https://meta13.com
 * Version: 1.5.6
 * License: GPL2+
 * License URI: https://www.gnu.org/licenses/gpl-2.0.txt
 *
 * @package CGB
 */

// Exit if accessed directly.
if ( ! defined( 'ABSPATH' ) ) {
	exit;
}

/**
 * Block Initializer.
 */
require_once plugin_dir_path( __FILE__ ) . 'src/init.php';
